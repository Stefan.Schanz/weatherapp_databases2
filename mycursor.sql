DECLARE @cityname VARCHAR(20);
DECLARE @lon float;
DECLARE @lat float;

DECLARE Schanz_cursorcity CURSOR
FOR
SELECT DISTINCT d.coord_lon, d.coord_lat, c.name from WeatherApp_weather_data as d
JOIN WeatherApp_city c on d.city_id = c.id

OPEN Schanz_cursorcity
FETCH NEXT FROM Schanz_cursorcity INTO
@lon, @lat, @cityname;

WHILE @@FETCH_STATUS = 0
BEGIN
	PRINT 'City-Name: ' + @cityname + ' Longitude: '+CAST(@lon AS varchar(7)) + ' Latitude: '+CAST(@lat AS varchar(7));
	FETCH NEXT FROM Schanz_cursorcity INTO @lon, @lat, @cityname;
END
CLOSE Schanz_cursorcity;
DEALLOCATE Schanz_cursorcity;